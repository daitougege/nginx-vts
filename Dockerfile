# 注意，本 dockefile 需要docker版本>=20.xx 的环境
FROM alpine:3.20.0
LABEL maintainer="GinSkeleton<1990850157@qq.com>" version="1.57" license="MIT"

# 指定安装目录、相关软件的版本号
ENV \
    install_dir=/usr/local/nginx \
    nginx_version=1.26.0  \
    nginx_vts_version=0.2.2  \
    pcre_version=10.43


WORKDIR ${install_dir}

COPY ./conf/nginx.conf  ./nginx/nginx-${nginx_version}.tar.gz   ./nginx_module_vts/nginx-module-vts-${nginx_vts_version}.tar.gz ./pcre/pcre2-${pcre_version}.tar.gz /tmp/

# 修改镜像源为国内镜像地址
RUN		sed   -i   's/http/#http/g'  /etc/apk/repositories  \
		&&  sed   -i  '$ahttp://mirrors.ustc.edu.cn/alpine/v3.20/main'  /etc/apk/repositories   \
		&&  sed   -i  '$ahttp://mirrors.ustc.edu.cn/alpine/v3.20/community'  /etc/apk/repositories  \
		&&  sed   -i  '$ahttps://mirrors.tuna.tsinghua.edu.cn/alpine/v3.20/main'  /etc/apk/repositories   \
		&&  sed   -i  '$ahttps://mirrors.tuna.tsinghua.edu.cn/alpine/v3.20/community'  /etc/apk/repositories   \
		&&  mkdir -p  /usr/local/nginx/logs/  \
		&&  addgroup  -S www  \
		&&  adduser www  -H -S -s /bin/sh  -D -G www  \
		&&  apk update \
		&&  set -ex \
		&&  apk add --no-cache  -U tzdata   pcre-dev openssl-dev \
		&&  apk add --no-cache  --virtual .build-deps \
            gcc  \
            g++  \
            make \
            musl-dev \
            zlib\
            zlib-dev \
            linux-headers \
            autoconf \
            gd-dev \
            libxslt-dev gnupg \
		&& cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
		&& echo "Asia/shanghai" > /etc/timezone \
		&&  cd /tmp/  \
		&&  tar  -zxf  nginx-${nginx_version}.tar.gz                      \
		&&  tar  -zxf  nginx-module-vts-${nginx_vts_version}.tar.gz       \
		&&  tar  -zxf  pcre2-${pcre_version}.tar.gz                        \
		&&  cd /tmp/nginx-${nginx_version}  \
		&&  ./configure --user=www --group=www --prefix=${install_dir} --with-http_stub_status_module --with-http_ssl_module  --with-http_v2_module --with-http_gzip_static_module --with-stream --with-http_sub_module  --with-pcre=/tmp/pcre2-${pcre_version}   --add-module=/tmp/nginx-module-vts-${nginx_vts_version}  \
		&&  make && make install  \
		&& cp /tmp/nginx.conf   ${install_dir}/conf/nginx.conf \
        && ln -sf /dev/stdout   /usr/local/nginx/logs/access.log  \
        && ln -sf /dev/stderr    /usr/local/nginx/logs/error.log  \
		&& rm -rf  /tmp/*   \
		&& apk del .build-deps

VOLUME ["/usr/local/nginx/logs"]

EXPOSE 80

ENV PATH $PATH:${install_dir}/sbin

CMD ["nginx","-g","daemon off;"]
