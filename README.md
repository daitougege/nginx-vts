# nginx-vts

#### 介绍
基于alpine:3.20 版本自定义编译,集成 nginx-vts 监控指标模块.

#### 快速构建镜像
```code   

# 上传本项目或者clone本项目，在  dockefile 同目录执行
# zhangqifeng/nginx_vts:v1.57  可以自行替换
docker   build   -t   zhangqifeng/nginx_vts:v1.57  .

```

#### 快速启动一个测试项目
```code
# 快速启动一个nginx默认项目
docker   run  --name   test-nginx-vts  -d    -p  24001:80  zhangqifeng/nginx_vts:v1.57

# 测试
curl http://127.0.0.1:24001/

```

#### 快速启动一个前端项目
```code   

docker   run  --name   project_name_frontend    -d \
    -p  20185:80  \
    -v   /root/test_options/nginx_conf/nginx.conf:/usr/local/nginx/conf/nginx.conf   \
    -v   /home/wwwroot/project2021_second_half/selfhelp_machine/dist_pro:/usr/local/nginx/html/   \
    -v   /home/wwwroot/project2021_second_half/selfhelp_machine/logs/:/usr/local/nginx/logs/   \
    zhangqifeng/nginx_vts:v1.57

```

#### 快速启动一个后端项目
```code
# 后端项目需要将 backend_nginx.conf 的内容覆盖  nginx.conf 的内容, 具体代理的接口地址请自行修改。

docker   run  --name   project_name_backend    -d \
    -p  20185:80  \
    -v   /root/test_options/nginx_conf/backend_nginx.conf:/usr/local/nginx/conf/nginx.conf   \
    -v   /home/wwwroot/project2021_second_half/selfhelp_machine/dist_pro:/usr/local/nginx/html/   \
    -v   /home/wwwroot/project2021_second_half/selfhelp_machine/logs/:/usr/local/nginx/logs/   \
    zhangqifeng/nginx_vts:v1.57

```
